//
//  PloggItem.swift
//  Plogge
//
//  Created by Mike Grankin on 5/17/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import CoreLocation

class PloggItem: NSObject {
    var count = 0
    var coordinates = CLLocationCoordinate2D()
    var date = Date()
    
    init(count: Int, coordinates: CLLocationCoordinate2D) {
        self.count = count
        self.coordinates = coordinates
        self.date = Date()
    }
}
