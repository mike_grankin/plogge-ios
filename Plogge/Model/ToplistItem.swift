//
//  ToplistItem.swift
//  Plogge
//
//  Created by Mike Grankin on 5/24/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import SwiftyJSON

class ToplistItem: NSObject {
    var name = ""
    var plogs = 0

    init?(jsonRaw: Any) {
        let json = JSON(jsonRaw)
        
        guard let name = json["name"].string,
        let plogs = json["plogs"].int
            else {
                return nil
        }
        
        self.name = name.capitalized
        self.plogs = plogs
    }

}
