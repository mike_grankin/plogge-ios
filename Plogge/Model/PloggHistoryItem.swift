//
//  PloggHistoryItem.swift
//  Plogge
//
//  Created by Mike Grankin on 5/22/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import SwiftyJSON

class PloggHistoryItem: NSObject {
    
    var id = 0
    var startTime = Date()
    var duration = ""
    var distance = 0
    var quantity = 0
    
    init?(jsonRaw: Any) {
        let json = JSON(jsonRaw)
        
        guard let id = json["id"].int,
            let startTimeISO = json["start_time"].string,
            let startTime = startTimeISO.dateFromISO8601,
            let duration = json["duration"].int,
            let distance = json["distance"].int,
            let quantity = json["quantity"].int
            else {
                return nil
        }
        
        self.id = id
        self.startTime = startTime
        self.duration = duration.durationText
        self.distance = distance
        self.quantity = quantity
    }
}
