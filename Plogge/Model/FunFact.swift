//
//  FunFact.swift
//  Plogge
//
//  Created by Mike Grankin on 5/23/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import SwiftyJSON

class FunFact: NSObject {
    var afterPlog = 0
    var message = ""

    init?(jsonRaw: Any) {
        let json = JSON(jsonRaw)
        
        guard let afterPlog = json["after_plog"].int,
            let message = json["message"].string
            else {
                return nil
        }
        
        self.afterPlog = afterPlog
        self.message = message
    }
}
