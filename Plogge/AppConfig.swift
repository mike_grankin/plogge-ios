//
//  AppConfig.swift
//  Plogge
//
//  Created by Mike Grankin on 4/27/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import KeychainSwift

class AppConfig: NSObject {
    static var instance = AppConfig()
    let keychain = KeychainSwift()
    var baseURL: String = ""
    {
        didSet{
                keychain.set(baseURL, forKey: "baseURL", withAccess: .accessibleAlways)
        }
    }
    
//    var appConfigState:AppConfigState = .notUpdated
    
    override init(){
        super.init()
        if let baseURL = keychain.get("baseURL")
        {
            self.baseURL = baseURL
        }
        else
        {
            self.baseURL = Constants.baseURLProd
        }
    }
    
    func useProdBaseURL()
    {
        self.baseURL = Constants.baseURLProd
    }

    func useTestBaseURL()
    {
        self.baseURL = Constants.baseURLTest
    }
}

//enum AppConfigState : Int {
//    case notUpdated
//    case isDownloading
//    case updated
//}

