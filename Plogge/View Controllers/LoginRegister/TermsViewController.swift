//
//  RegisterInitialViewController.swift
//  Plogge
//
//  Created by Mike Grankin on 5/14/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import UIKit
import SafariServices
import PKHUD

class TermsViewController: UIViewController, SFSafariViewControllerDelegate {
    @IBOutlet var acceptTermsSwitch: UISwitch!
    @IBOutlet weak var agreementLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func acceptButtonPressed(_ sender: AnyObject) {
        var alertText = ""
        
        if acceptTermsSwitch.isOn != true
        {
            alertText += "termsNotAcceptedMessage".localized()
            agreementLabel.textColor = .red
        }
        
        if alertText == "" {
            HUD.flash(.progress, delay: 60.0)
            
            Auth.instance.acceptTerms(true) { (success, errorMessage) in
                if success
                {
                    HUD.flash(.success, onView: nil, delay: 1.0){ (result) in
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.launchStoryboard(StoryboardName.Main)
                    }
                } else {
                    HUD.flash(.error, onView: nil, delay: 1.0){ (result) in
                        let alert = UIAlertController(title: "error".localized(), message: errorMessage, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        else
        {
            HUD.flash(.error, onView: nil, delay: 1.0){ (result) in
                let alert = UIAlertController(title: "error".localized(), message: alertText, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    @IBAction func didTapUserAgreement(_ sender: UIButton) {
        if let linkUrl = URL.init(string: "https://ways.no/plogger-personvernerklaering/")
        {
            let safariViewController = SFSafariViewController(url: linkUrl)
            safariViewController.delegate = self
//            safariViewController.preferredBarTintColor = UIColor.init(r: 7, g: 0, b: 39)
//            safariViewController.preferredControlTintColor = UIColor.white
//            safariViewController.modalPresentationCapturesStatusBarAppearance = true
            self.present(safariViewController, animated: true, completion: nil)
        }

    }
    
    @IBAction func doNotAcceptButtonPressed(_ sender: UIButton) {
        HUD.flash(.progress, delay: 60.0)
        
        Auth.instance.acceptTerms(false) { (success, errorMessage) in
            if success
            {
                HUD.flash(.success, onView: nil, delay: 1.0){ (result) in
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.launchStoryboard(StoryboardName.LoginRegister)
                }
            } else {
                HUD.flash(.error, onView: nil, delay: 1.0){ (result) in
                    let alert = UIAlertController(title: "error".localized(), message: errorMessage, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController)
    {
        controller.dismiss(animated: true, completion: nil)
        print("user agreement have been opened")
    }
}

