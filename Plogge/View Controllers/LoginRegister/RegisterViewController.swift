//
//  RegisterViewController.swift
//  Plogge
//
//  Created by Mike Grankin on 4/27/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import UIKit
import Async

class RegisterViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var passwordConfirmTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        usernameTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        passwordConfirmTextField.delegate = self
    }
    
    @IBAction func registerButtonPressed(_ sender: AnyObject) {
        view.endEditing(true)
        
        var alertText = ""
        
        if usernameTextField.text!.isEmpty{
            alertText += "emptyUsernameMessage".localized()
        }

        if emailTextField.text!.isEmpty || !emailTextField.text!.isEmail()
        {
            alertText += "emptyEmailMessage".localized()
        }

        if passwordTextField.text!.isEmpty || passwordConfirmTextField.text!.isEmpty
        {
            alertText += "emptyPasswordMessage".localized()
        }
        else if passwordTextField.text != passwordConfirmTextField.text!
        {
            alertText += "differentPasswordMessage".localized()
        }
        
        if alertText == "" {
            Auth.instance.register(username: usernameTextField.text!, email: emailTextField.text!, password: passwordTextField.text!){ (success, showTermsScreen, errorMessage) in
                if success
                {
                    if showTermsScreen
                    {
                        self.performSegue(withIdentifier: "CreateAccountToTermsSegue", sender: self)
                    }
                    else
                    {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.launchStoryboard(StoryboardName.Main)
                    }
                    
                } else {
                    let alert = UIAlertController(title: "error".localized(), message: errorMessage, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        else
        {
            let alert = UIAlertController(title: "error".localized(), message: alertText, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        Async.main {
            if textField == self.usernameTextField
            {
                self.emailTextField.becomeFirstResponder()
            }
            else if textField == self.emailTextField {
                self.passwordTextField.becomeFirstResponder()
            }
            else if textField == self.passwordTextField {
                self.passwordConfirmTextField.becomeFirstResponder()
            }
            else if textField == self.passwordConfirmTextField {
//                self.registerButtonPressed(textField)
                self.dismissKeyboard()
            }
        }
        return true
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
}

