//
//  ForgotPasswordViewController.swift
//  Plogge
//
//  Created by Mike Grankin on 4/27/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import UIKit
import Async

class ForgotPasswordViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        emailTextField.delegate = self
    }
    
    @IBAction func forgotButtonPressed(_ sender: AnyObject) {
        
        var alertText = ""
        
        if emailTextField.text!.isEmpty || !emailTextField.text!.isEmail()
        {
            alertText += "emptyEmailMessage".localized()
        }
        
        if alertText == "" {
            Auth.instance.forgotPassword(email: emailTextField.text!.trimmingCharacters(in: .whitespaces)) { (success, errorMessage) in
                if success
                {
                    let alert = UIAlertController(title: "forgotPasswordAlertTitle".localized(), message: "forgotPasswordAlertDescription".localized(), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: { (action) in
                        Async.main()
                            {
                                self.navigationController?.popToRootViewController(animated: true)
                        }
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    let alert = UIAlertController(title: "error".localized(), message: errorMessage, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        else
        {
            let alert = UIAlertController(title: "error".localized(), message: alertText, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        Async.main {
            if textField == self.emailTextField {
                self.dismissKeyboard()
            }
        }
        return true
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
}
