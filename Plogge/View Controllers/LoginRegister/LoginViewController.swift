//
//  LoginViewController.swift
//  Plogge
//
//  Created by Mike Grankin on 4/27/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import UIKit
import Async
import FacebookLogin
import FacebookCore
import GoogleSignIn

class LoginViewController: UIViewController, UITextFieldDelegate, GIDSignInUIDelegate, GIDSignInDelegate {
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: LoadingUIButton!
    @IBOutlet weak var loginFBButton: LoadingUIButton!
    @IBOutlet weak var loginGoogleButton: LoadingUIButton!
    
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
//#if DEBUG
        let quintupleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.showChangeServerAlert))
        quintupleTapRecognizer.numberOfTapsRequired = 5
        view.addGestureRecognizer(quintupleTapRecognizer)
//        emailTextField.text = "mike@ways.as"
//        passwordTextField.text = "test1234"
//#endif
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
//        phoneTextField.becomeFirstResponder()
        
        navigationController?.view.backgroundColor = UIColor.white //https://stackoverflow.com/questions/22413193/dark-shadow-on-navigation-bar-during-segue-transition-after-upgrading-to-xcode-5
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default) //https://stackoverflow.com/questions/26390072/remove-border-in-navigationbar-in-swift
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = UIColor.AppColors.darkBlue
        
        GIDSignIn.sharedInstance().clientID = Constants.GoogleClientID
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self

    }
    
    @IBAction func loginButtonPressed(_ sender: AnyObject) {
        view.endEditing(true)
        
        var alertText = ""
        
        if emailTextField.text!.isEmpty
        {
            alertText += "emptyEmailMessage".localized()
        }
        
        if passwordTextField.text!.isEmpty
        {
            alertText += " " + "emptyPasswordMessage".localized()
        }
        
        if alertText == "" {
            
            Async.main()
            {
                self.loginButton.showLoading()
            }
            
            Auth.instance.login(emailTextField.text!, passwordTextField.text!) { (success, showTermsScreen, errorMessage) in
                if success
                {
                    self.loginButton.hideLoading()
                    if showTermsScreen
                    {
                        self.performSegue(withIdentifier: "LoginToTermsSegue", sender: self)
                    }
                    else
                    {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.launchStoryboard(StoryboardName.Main)
                    }
                }
                else
                {
                    self.loginButton.hideLoading()
                    let alert = UIAlertController(title: "loginFailedAlertTitle".localized(), message: errorMessage, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        else
        {
            let alert = UIAlertController(title: "error".localized(), message: alertText, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func facebookButtonPressed(_ sender: Any) {
        
        Async.main(){
            self.loginFBButton.showLoading()
        }

        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [ReadPermission.publicProfile, ReadPermission.email], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                self.loginFBButton.hideLoading()
                let alert = UIAlertController(title: "error".localized(), message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case .cancelled:
                self.loginFBButton.hideLoading()
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                if !declinedPermissions.contains(Permission(name: "email")) && grantedPermissions.contains(Permission(name: "email"))
                {
                    self.getFBUserInfo(withFBAccessToken: accessToken)
                }
                else
                {
                    let alert = UIAlertController(title: "Login".localized(), message: "User do not allowed to use data from facebook", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }

    func getFBUserInfo(withFBAccessToken accessToken: AccessToken) {
        let request = GraphRequest(graphPath: "me", parameters: ["fields":"email,name, picture.width(200).height(200)"], accessToken: AccessToken.current, httpMethod: .GET, apiVersion: FacebookCore.GraphAPIVersion.defaultVersion)
        request.start { (response, result) in
            switch result {
            case .success(let value):
                self.loginWithFB(accessToken, value.dictionaryValue!)
            case .failed(let error):
                self.loginFBButton.hideLoading()
                let alert = UIAlertController(title: "error".localized(), message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func loginWithFB(_ accessToken:AccessToken, _ userDict: Dictionary<String, Any>)
    {
        var alertText = ""
        
        if userDict["email"] as? String == nil
        {
            alertText += "emptyEmailMessage".localized()
        }
        
        if alertText == "" {
            
            Auth.instance.loginWithFB(accessToken, userDict) { (success, showTermsScreen, errorMessage) in
                self.loginFBButton.hideLoading()
                if success
                {
                    if showTermsScreen
                    {
                        self.performSegue(withIdentifier: "LoginToTermsSegue", sender: self)
                    }
                    else
                    {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.launchStoryboard(StoryboardName.Main)
                    }
                }
                else
                {
                    let alert = UIAlertController(title: "loginFailedAlertTitle".localized(), message: errorMessage, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        else
        {
            self.loginFBButton.hideLoading()
            let alert = UIAlertController(title: "error".localized(), message: alertText, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func googleButtonPressed(_ sender: Any) {
        Async.main(){
            self.loginGoogleButton.showLoading()
        }
        GIDSignIn.sharedInstance().signIn()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        Async.main {
            if textField == self.emailTextField
            {
                self.passwordTextField.becomeFirstResponder()
            }
            else if textField == self.passwordTextField
            {
                self.loginButtonPressed(textField)
            }
        }
        return true
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @objc func showChangeServerAlert()
    {
        var isProd = true
        let currentBaseURL = AppConfig.instance.baseURL
        if currentBaseURL == Constants.baseURLTest
        {
            isProd = false
        }
        let alert = UIAlertController(title: "Select server", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Production", style: isProd ? .destructive : .default, handler: { (action) in
            AppConfig.instance.useProdBaseURL()
        }))
        alert.addAction(UIAlertAction(title: "Test", style: !isProd ? .destructive : .default, handler: { (action) in
            AppConfig.instance.useTestBaseURL()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            self.loginGoogleButton.hideLoading()
            let alert = UIAlertController(title: "error".localized(), message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            Auth.instance.loginWithGoogle(user) { (success, showTermsScreen, errorMessage) in
                self.loginGoogleButton.hideLoading()
                if success
                {
                    if showTermsScreen
                    {
                        self.performSegue(withIdentifier: "LoginToTermsSegue", sender: self)
                    }
                    else
                    {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.launchStoryboard(StoryboardName.Main)
                    }
                }
                else
                {
                    let alert = UIAlertController(title: "loginFailedAlertTitle".localized(), message: errorMessage, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        self.loginGoogleButton.hideLoading()
        let alert = UIAlertController(title: "error".localized(), message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

