//
//  OnboardingPageViewController.swift
//  Plogge
//
//  Created by Mike Grankin on 6/4/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import Foundation
import UIKit

class OnboardingPageViewController: UIPageViewController{
    
    weak var onboardingDelegate: OnboardingPageViewControllerDelegate?
    
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        return [self.newOrderedViewController("First"),
                self.newOrderedViewController("Second"),
                self.newOrderedViewController("Third"),]
    }()
    
    private func newOrderedViewController(_ order: String) -> UIViewController {
        return UIStoryboard(name: StoryboardName.Onboarding.rawValue, bundle: nil) .
            instantiateViewController(withIdentifier: "\(order)OnboardingViewController")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.view.backgroundColor = UIColor.init(r: 239, g: 239, b: 244)
        
        dataSource = self
        delegate = self
        
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
        
        onboardingDelegate?.onboardingPageViewController(self, didUpdatePageCount: orderedViewControllers.count)
    }
    
    func loadNextViewController()
    {
        if let currViewController = self.viewControllers?.first,
            let currIndex = self.orderedViewControllers.index(of: currViewController),
            currIndex+1 < self.orderedViewControllers.count
        {
            let nextViewController = orderedViewControllers[currIndex+1]
//            let nextViewController = orderedViewControllers.item(at: currIndex+1)
            setViewControllers([nextViewController],
                               direction: .forward,
                               animated: true,
                               completion:{ (finished) in
                                self.updateSelectedPageIndex()
            })
        }
        
    }
    
    func loadPrevViewController()
    {
        if let currViewController = self.viewControllers?.first,
            let currIndex = self.orderedViewControllers.index(of: currViewController),
            currIndex > 0
        {
            let prevViewController = orderedViewControllers[currIndex-1]
            setViewControllers([prevViewController],
                               direction: UIPageViewControllerNavigationDirection.reverse,
                               animated: true,
                               completion:{ (finished) in
                                self.updateSelectedPageIndex()
            })
        }
        
    }
    
    func loadLastViewController()
    {
        if let lastViewController = orderedViewControllers.last {
            
            setViewControllers([lastViewController],
                               direction: .forward,
                               animated: true,
                               completion:{ (finished) in
                                self.updateSelectedPageIndex()
            }
            )
        }
    }
    
    func updateSelectedPageIndex()
    {
        if let currViewController = self.viewControllers?.first,
            let index = self.orderedViewControllers.index(of: currViewController)
        {
            self.onboardingDelegate?.onboardingPageViewController(self,
                                                                  didUpdatePageIndex: index)
        }
    }
}

extension OnboardingPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
}

extension OnboardingPageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController],
                            transitionCompleted completed: Bool) {
        updateSelectedPageIndex()
    }
}

protocol OnboardingPageViewControllerDelegate: class {
    
    /**
     Called when the number of pages is updated.
     
     - parameter onboardingPageViewController: the OnboardingPageViewController instance
     - parameter count: the total number of pages.
     */
    func onboardingPageViewController(_ onboardingPageViewController: OnboardingPageViewController,
                                      didUpdatePageCount count: Int)
    
    /**
     Called when the current index is updated.
     
     - parameter onboardingPageViewController: the OnboardingPageViewController instance
     - parameter index: the index of the currently visible page.
     */
    func onboardingPageViewController(_ onboardingPageViewController: OnboardingPageViewController,
                                      didUpdatePageIndex index: Int)
    
}
