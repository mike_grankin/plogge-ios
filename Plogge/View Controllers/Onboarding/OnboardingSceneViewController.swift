//
//  OnboardingSceneViewController.swift
//  Plogge
//
//  Created by Mike Grankin on 6/4/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import Foundation
import UIKit

class OnboardingSceneViewController: UIViewController
{
    var imageView:UIImageView?
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView = view.viewWithTag(111) as! UIImageView?
//        imageView = view.subviews.filter{$0 is UIImageView}.first        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        imageView?.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        imageView?.alpha = 0.3
        let duration = 0.7
        let delay = 0.2
        UIView.animate(withDuration: duration + delay,
                       animations: {
                        self.imageView?.alpha = 1.0
                        },
                       completion: nil
        )
        
        UIView.animate(withDuration: duration,
                       delay: delay,
                       usingSpringWithDamping: CGFloat(0.40),
                       initialSpringVelocity: CGFloat(7.0),
                       options:  [.curveEaseIn, .allowUserInteraction],
                       animations: {
                        self.imageView?.transform = CGAffineTransform.identity
                        },
                       completion: nil
        )

    }

}
