//
//  OnboardingViewController.swift
//  Plogge
//
//  Created by Mike Grankin on 6/4/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import Foundation
import UIKit
import Async

class OnboardingViewController: UIViewController{
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet var skipButton: UIButton!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var nextButton: UIButton!
    weak var onboardingPageViewController: OnboardingPageViewController!
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    func updateControlls() {
        Async.main{
            let isLastPage = self.pageControl.currentPage == self.pageControl.numberOfPages-1
//            self.skipButton.isHidden = isLastPage
//            self.nextButton.isHidden = isLastPage
            if isLastPage
            {
                self.skipButton.setTitle("close".localized(), for: .normal)
                self.nextButton.setTitle("done".localized(), for: .normal)
            }
            else
            {
                self.skipButton.setTitle("skip".localized(), for: .normal)
                self.nextButton.setTitle("next".localized(), for: .normal)
            }
            
//            let isFirstPage = self.pageControl.currentPage == 0
//            self.backButton.isHidden = isFirstPage
//            self.updatePageControl()
        }
        
    }
//    func updatePageControl() {
//        for (index, dot) in self.pageControl.subviews.enumerated() {
//            if index == self.pageControl.currentPage {
//                dot.backgroundColor = UIColor(r: 44, g: 50, b: 123, alpha: 0.5)
//                dot.layer.cornerRadius = dot.frame.size.height / 2;
//            } else {
//                dot.backgroundColor = UIColor(r: 44, g: 50, b: 123)
//                dot.layer.cornerRadius = dot.frame.size.height / 2
////                dot.layer.borderColor = UIColor.init(r: 95, g: 95, b: 95).cgColor
////                dot.layer.borderColor = UIColor.init(r: 227, g: 233, b: 242).cgColor
////                dot.layer.borderWidth = 2
//            }
//        }
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let onboardingPageViewControllerUnwrapped = segue.destination as? OnboardingPageViewController {
            self.onboardingPageViewController = onboardingPageViewControllerUnwrapped
            self.onboardingPageViewController?.onboardingDelegate = self
        }
    }
    
    @IBAction func didTapSkipButton(_ sender: Any)
    {
        let isLastPage = self.pageControl.currentPage == self.pageControl.numberOfPages-1
        if isLastPage
        {
            self.completeOnboarding()
        }
        else
        {
            self.onboardingPageViewController.loadLastViewController()
        }
    }

    @IBAction func didTapBackButton(_ sender: Any)
    {
        Async.main{
            if self.pageControl.currentPage > 0
            {
                self.onboardingPageViewController.loadPrevViewController()
            }
        }
        
    }

    @IBAction func didTapNextButton(_ sender: Any)
    {
        Async.main{
            if self.pageControl.currentPage < self.pageControl.numberOfPages-1
            {
                self.onboardingPageViewController.loadNextViewController()
            }
            else
            {
                self.completeOnboarding()
            }
        }
    }
    
    func completeOnboarding()
    {
        Defaults.groupDefaults().set(true, forKey: onboardingKey)
        //now load the main storyboard
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.launchStoryboard(StoryboardName.LoginRegister)
    }

}

extension OnboardingViewController: OnboardingPageViewControllerDelegate {
    
    func onboardingPageViewController(_ onboardingPageViewController: OnboardingPageViewController,
                                    didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
        self.updateControlls()
    }
    
    func onboardingPageViewController(_ onboardingPageViewController: OnboardingPageViewController,
                                    didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
        self.updateControlls()
    }
    
}
