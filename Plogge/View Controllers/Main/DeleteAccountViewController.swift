//
//  DeleteAccountViewController.swift
//  Plogge
//
//  Created by Mike Grankin on 5/30/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import UIKit
import Async

class DeleteAccountViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func deleteButtonPressed(_ sender: AnyObject) {
        Auth.instance.deleteAccount(){ (success, errorMessage) in
            if success
            {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.launchStoryboard(StoryboardName.LoginRegister)
            }
            else
            {
                let alert = UIAlertController(title: "error".localized(), message: errorMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}
