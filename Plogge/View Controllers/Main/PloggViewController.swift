//
//  PloggViewController.swift
//  Plogge
//
//  Created by Mike Grankin on 5/15/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import UIKit
import CoreLocation
import Async
import CoreMotion
import SafariServices

class PloggViewController: UIViewController, CLLocationManagerDelegate, SFSafariViewControllerDelegate {
    @IBOutlet weak var notStartedView: UIView!
    @IBOutlet weak var startedView: UIView!
    @IBOutlet weak var finishedView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var ploggsCountLabel: UILabel!
    @IBOutlet weak var funFactLabel: UILabel!
    @IBOutlet weak var finishedTimeLabel: UILabel!
    @IBOutlet weak var finishedDistanceLabel: UILabel!
    @IBOutlet weak var finishedPloggsCountLabel: UILabel!
    @IBOutlet weak var funFactStackView: UIStackView!
    @IBOutlet weak var startButton: LoadingUIButton!
    
    let locationManager = CLLocationManager()
    var state:PloggingState = .notStarted
    var startTime = Date()
    var distance = 0
    var pickerPloggsCount = 0
    var timer: Timer?
    var pedometer: CMPedometer?
    var currentPlogsCount = 0
    var lastLocation: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        notStartedView.isHidden = false
        startedView.isHidden = true
        finishedView.isHidden = true
        updateUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func updateUI()
    {
        Async.main{
            self.ploggsCountLabel.text = String(self.currentPlogsCount)
            self.finishedPloggsCountLabel.text = String(self.currentPlogsCount)
            
            let dateFormatter = DateFormatter()
            let userCalendar = Calendar.current
            let requestedComponent: Set<Calendar.Component> = [.hour,.minute,.second]
            let timeDifference = userCalendar.dateComponents(requestedComponent, from: self.startTime, to: Date())
            let date = Calendar(identifier: .gregorian).date(from: timeDifference)
            dateFormatter.dateFormat = "HH:mm:ss"
            let dateString =  dateFormatter.string(from: date!)
            
            let userSessionTimeString = dateString
            
            self.timeLabel.text = userSessionTimeString
            self.finishedTimeLabel.text = userSessionTimeString
            let distance = Int(self.distance)
            self.distanceLabel.text = distance.kmFormatted
            self.finishedDistanceLabel.text = distance.kmFormatted
        }
    }
    
    @IBAction func didTapStartButton(_ sender: Any) {
        if self.state == .notStarted
        {
            self.startButton.showLoading()
            self.state = .startRequested
            self.currentPlogsCount = 0
            self.distance = 0
            self.locationManager.requestWhenInUseAuthorization()
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startUpdatingLocation()
            }
            
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateUI), userInfo: nil, repeats: true)
            self.startTime = Date()
            
            self.pedometer = CMPedometer()
            self.pedometer!.startUpdates(from: startTime) { (pedometerData , error) in
                guard let pedometerData = pedometerData, error == nil else { return }
                self.distance = (pedometerData.distance?.intValue)!
                self.updateUI()
            }
        }
    }

    @IBAction func didTapAddOnePloggButton(_ sender: Any) {
        self.addPloggs()
    }

    @IBAction func didTapAddPloggsButton(_ sender: Any) {
        self.showPloggsPicker()
    }
    
    @IBAction func didTapStopButton(_ sender: Any) {
        Plog.instance.stop(self.lastLocation!.coordinate, distance){ (success, message) in
            if success
            {
                if self.timer != nil
                {
                    self.timer!.invalidate()
                    self.timer = nil
                }
                self.pedometer = nil

                self.state = .stopped
                Async.main{
                    self.notStartedView.isHidden = true
                    self.startedView.isHidden = true
                    self.finishedView.isHidden = false
                    if let funFactText = message
                    {
                        self.showFunFact(text: funFactText)
                    }
                    self.updateUI()
                }
            }
            else
            {
                let alert = UIAlertController(title: "error".localized(), message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }

    }
    
    @IBAction func didTapRemoveLastPloggButton(_ sender: Any) {
        if let removedPloggsCount = Plog.instance.removeLastPlogg()
        {
            self.currentPlogsCount -= removedPloggsCount
        }
        else
        {
            self.currentPlogsCount = 0
        }
        self.updateUI()
    }
    
    func addPloggs(_ plogsCount:Int = 1)
    {
        self.currentPlogsCount += plogsCount
        let plogg = PloggItem(count: plogsCount, coordinates: self.lastLocation!.coordinate)
        Plog.instance.addPlogg(plogg)
        self.updateUI()
        if let funFactText = Plog.instance.getFunFact(afterPlog: currentPlogsCount)
        {
            showFunFact(text: funFactText)
        }
    }

    @IBAction func didTapNewPloggButton(_ sender: Any) {
        self.state = .notStarted
        notStartedView.isHidden = false
        startedView.isHidden = true
        finishedView.isHidden = true
    }
    
    func showFunFact(text:String)
    {
        Async.main{
            self.funFactStackView.isHidden = false
            self.funFactLabel.text = text
        }
        Async.main(after: 5.0)
        {
            self.funFactStackView.isHidden = true
            self.funFactLabel.text = ""
        }
    }
    
    func showPloggsPicker()
    {
        let alert = UIAlertController(style: .actionSheet, title: nil, message: nil)
        
        
        let goalValues: [Int] = (0...99).map { Int($0) }
        let pickerViewValues: [[String]] = [goalValues.map { Int($0).description }]
        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: 0)
        
        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) {  vc, picker, index, values in
            self.pickerPloggsCount = goalValues[index.row]
        }
        
        alert.addAction(title: "add".localized(), style: UIAlertActionStyle.default, handler: { _ in
            if self.pickerPloggsCount > 0
            {
                self.addPloggs(self.pickerPloggsCount)
                self.pickerPloggsCount = 0
            }
        })
        
        alert.addAction(title: "cancel".localized(), style: UIAlertActionStyle.cancel)
        alert.show()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last
        {
            handleLocationUpdate(location)
        }
    }
    
    func handleLocationUpdate(_ location: CLLocation)
    {
        lastLocation = location
        switch state {
        case .startRequested:
            self.state = .starting
            Plog.instance.start(location.coordinate){ (success, message) in
                if success
                {
                    self.state = .started
                    Async.main{
                        self.notStartedView.isHidden = true
                        self.startedView.isHidden = false
                        self.updateUI()
                        self.startButton.hideLoading()
                    }
                }
                else
                {
                    self.state = .notStarted
                    let alert = UIAlertController(title: "error".localized(), message: message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        default:
            _ = true
        }
    }
    
    @IBAction func didTapFriidrettButton(_ sender: Any) {
        self.handleURL("https://www.friidrett.no/")
    }
    
    @IBAction func didTapGjensidigeButton(_ sender: Any) {
        self.handleURL("https://www.gjensidige.no/")
    }
    
    
    func handleURL(_ urlString:String)
    {
        if let linkUrl = URL.init(string: urlString)
        {
            let safariViewController = SFSafariViewController(url: linkUrl)
            safariViewController.delegate = self
            self.present(safariViewController, animated: true, completion: nil)
        }
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController)
    {
        controller.dismiss(animated: true, completion: nil)
    }
}

enum PloggingState
{
    case notStarted
    case startRequested
    case starting
    case started
    case stopped
}
