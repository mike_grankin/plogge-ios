//
//  ProfileViewController.swift
//  Plogge
//
//  Created by Mike Grankin on 5/29/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import UIKit
import SafariServices
import Async

class ProfileViewController: UIViewController, SFSafariViewControllerDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var statisticLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var items = ["profileUsername".localized(), "profilePassword".localized(), "logOut".localized(), "profileDelete".localized()]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.view.backgroundColor = UIColor.white //https://stackoverflow.com/questions/22413193/dark-shadow-on-navigation-bar-during-segue-transition-after-
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default) //https://stackoverflow.com/questions/26390072/remove-border-in-navigationbar-in-swift
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        tableView.tableFooterView = UIView(frame: .zero)

        Auth.instance.downloadProfile{ (success, errorMessage) in
            if success
            {
                self.updateUI()
            }
            else
            {
                let alert = UIAlertController(title: "error".localized(), message: errorMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateUI()
    }

    func updateUI()
    {
        Async.main{
            self.usernameLabel.text = Auth.instance.profileUsername
            self.emailLabel.text = Auth.instance.profileEmail
            let statisticTextTemplate = "profileStatisticText".localized()
            let statisticText = String(format: statisticTextTemplate, Auth.instance.profileTotalPlogs, Auth.instance.profileTotalDistance.kmFormatted)
            self.statisticLabel.text = statisticText
        }
    }
    
    @IBAction func didTapShareButton(_ sender: Any) {
        let shareTextTemplate = "profileShareText".localized()
        let shareText = Constants.itunesLink + "\n" + String(format: shareTextTemplate, Auth.instance.profileTotalPlogs, Auth.instance.profileTotalDistance.kmFormatted)
        let activityItems = [shareText]
//        let shareText = String(format: shareTextTemplate, Auth.instance.profileTotalPlogs, Auth.instance.profileTotalDistance.kmFormatted)
//        let activityItems = [Constants.itunesLink, shareText]
        let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        self.present(activityController, animated: true, completion: nil)
    }
    
    @IBAction func didTapFriidrettButton(_ sender: Any) {
        self.handleURL("https://www.friidrett.no/")
    }
    
    @IBAction func didTapGjensidigeButton(_ sender: Any) {
        self.handleURL("https://www.gjensidige.no/")
    }
    
    @IBAction func didTapPrivacyPolicyButton(_ sender: Any) {
        self.handleURL("https://ways.no/plogger-personvernerklaering/")
    }
    
    func handleURL(_ urlString:String)
    {
        if let linkUrl = URL.init(string: urlString)
        {
            let safariViewController = SFSafariViewController(url: linkUrl)
            safariViewController.delegate = self
            self.present(safariViewController, animated: true, completion: nil)
        }
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController)
    {
        controller.dismiss(animated: true, completion: nil)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let itemText = items[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath)
        cell.textLabel?.text = itemText        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 0:
            performSegue(withIdentifier: "ProfileToChangeUsernameSegue", sender: self)
        case 1:
            performSegue(withIdentifier: "ProfileToChangeEmailSegue", sender: self)
        case 2:
            Auth.instance.logOut() { (success, errorMessage) in
                if success
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.launchStoryboard(StoryboardName.LoginRegister)
                }
                else
                {
                    let alert = UIAlertController(title: "error".localized(), message: errorMessage, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        case 3:
            performSegue(withIdentifier: "ProfileToDeleteAccountSegue", sender: self)
        default:
            _ = false
        }
    }
    
    //https://stackoverflow.com/a/41618459/2937896
    //    func tableView( _ tableView: UITableView, heightForHeaderInSection section: Int ) -> CGFloat
    //    {
    //        switch section {
    //        case 1:
    //            return 18.0
    //        default:
    //            return 38.0//CGFloat.leastNormalMagnitude
    //        }
    //    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}
