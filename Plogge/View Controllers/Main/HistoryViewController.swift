//
//  HistoryViewController.swift
//  Plogge
//
//  Created by Mike Grankin on 5/22/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import UIKit
import Async

class HistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, HistoryCellDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var ploggHistoryItems = Array<PloggHistoryItem>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        let refreshControl = UIRefreshControl.init()
        refreshControl.addTarget(self, action: #selector(self.handlePullToRefresh), for: UIControlEvents.valueChanged)
        self.tableView.refreshControl = refreshControl

        // Do any additional setup after loading the view.
        self.downloadHistory()
    }

    @objc func handlePullToRefresh()
    {
        self.downloadHistory(true)
    }
    
    func downloadHistory(_ isFromRefreshController:Bool = false)
    {
        Plog.instance.history{ (success, errorMessage) in
            if success
            {
                self.refreshData()
            }
            else
            {
                let alert = UIAlertController(title: "error".localized(), message: errorMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }

            if isFromRefreshController
            {
                self.tableView.refreshControl?.endRefreshing()
            }
        }
    }
    func refreshData()
    {
        self.ploggHistoryItems = Plog.instance.ploggHistoryItems

        Async.main{
            UIView.transition(with: self.tableView, duration: 0.25, options: .transitionCrossDissolve, animations: { () -> Void in
                self.tableView.reloadData()
            }, completion: nil)
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ploggHistoryItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let ploggHistoryItem = ploggHistoryItems[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath) as! HistoryCell
        cell.delegate = self
        cell.ploggHistoryItem = ploggHistoryItem
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let startTimeString = dateFormatter.string(from: ploggHistoryItem.startTime)
        
        cell.dateLabel?.text = startTimeString + " " + ploggHistoryItem.duration
        let distanceString = ploggHistoryItem.distance.kmFormatted
        cell.descriptionLabel.text = "\(distanceString) plogget - \(ploggHistoryItem.quantity) plogger plukket"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: - HistoryCellDelegate methods -
    func didTapDeleteButton(forItem ploggHistoryItem: PloggHistoryItem) {
        
        let alert = UIAlertController(title: "delete".localized(), message: "deleteHistoryItemMessage".localized(), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "delete".localized(), style: .destructive, handler: { (action) in
            Plog.instance.delete(ploggHistoryItem){ (success, message) in
                if success
                {
                    self.refreshData()
                }
                else
                {
                    let alert = UIAlertController(title: "error".localized(), message: message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        }))
        alert.addAction(UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func didTapShareButton(forItem ploggHistoryItem: PloggHistoryItem) {
        let shareTextTemplate = "shareText".localized()
        let shareText = Constants.itunesLink + "\n" + String(format: shareTextTemplate, ploggHistoryItem.distance.kmFormatted, ploggHistoryItem.quantity)
        let activityItems = [shareText]
        let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        self.present(activityController, animated: true, completion: nil)
    }

}

class HistoryCell: UITableViewCell {
    
    //Outlets
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var delegate:HistoryCellDelegate!
    var ploggHistoryItem:PloggHistoryItem?
    
    @IBAction func didTapDeleteButton(_ sender: Any) {
        if let ploggHistoryItem = ploggHistoryItem
        {
            delegate.didTapDeleteButton(forItem: ploggHistoryItem)
        }
    }
    
    @IBAction func didTapShareButton(_ sender: Any) {
        if let ploggHistoryItem = ploggHistoryItem
        {
            delegate.didTapShareButton(forItem: ploggHistoryItem)
        }
    }
}

protocol HistoryCellDelegate {
    func didTapDeleteButton(forItem ploggHistoryItem: PloggHistoryItem)
    func didTapShareButton(forItem ploggHistoryItem: PloggHistoryItem)
}

