//
//  ToplistViewController.swift
//  Plogge
//
//  Created by Mike Grankin on 5/24/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import UIKit
import Async
import SafariServices

class ToplistViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SFSafariViewControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var placementLabel: UILabel!
    @IBOutlet weak var communeLabel: UILabel!
    
    var toplistItems = Array<ToplistItem>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.view.backgroundColor = UIColor.white //https://stackoverflow.com/questions/22413193/dark-shadow-on-navigation-bar-during-segue-transition-after-
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default) //https://stackoverflow.com/questions/26390072/remove-border-in-navigationbar-in-swift
        self.navigationController?.navigationBar.shadowImage = UIImage()

        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        let refreshControl = UIRefreshControl.init()
        refreshControl.addTarget(self, action: #selector(self.handlePullToRefresh), for: UIControlEvents.valueChanged)
        self.tableView.refreshControl = refreshControl
        
        self.downloadToplist()
    }
    
    @IBAction func didTapFriidrettButton(_ sender: Any) {
        self.handleURL("https://www.friidrett.no/")
    }
    
    @IBAction func didTapGjensidigeButton(_ sender: Any) {
        self.handleURL("https://www.gjensidige.no/")
    }
    
    @objc func handlePullToRefresh()
    {
        self.downloadToplist(true)
    }
    
    func downloadToplist(_ isFromRefreshController:Bool = false)
    {
        Scoreboard.instance.toplist{ (success, errorMessage) in
            if success
            {
                self.refreshData()
            }
            else
            {
                let alert = UIAlertController(title: "error".localized(), message: errorMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
            if isFromRefreshController
            {
                self.tableView.refreshControl?.endRefreshing()
            }
        }
    }
    func refreshData()
    {
        self.toplistItems = Scoreboard.instance.toplistItems
        
        Async.main{
            UIView.transition(with: self.tableView, duration: 0.25, options: .transitionCrossDissolve, animations: { () -> Void in
                self.tableView.reloadData()
                let placementTextTemplate = "toplistPlacementText".localized()
                let placementText = String(format: placementTextTemplate, Scoreboard.instance.toplistUserRanking, Scoreboard.instance.toplistTotalUsers)
                self.placementLabel.text = placementText
                let communeTextTemplate = "toplistCommuneText".localized()
                let communeText = String(format: communeTextTemplate, Scoreboard.instance.toplistUserCommune, Scoreboard.instance.toplistCommunePlogs.kFormatted)
                self.communeLabel.text = communeText

            }, completion: nil)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toplistItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let toplistItem = toplistItems[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "toplistCell", for: indexPath) as! ToplistCell
        
        cell.orderNoLabel.text = String(indexPath.row+1)
        cell.nameLabel.text = toplistItem.name
        cell.plogsLabel.text = toplistItem.plogs.kFormatted
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func handleURL(_ urlString:String)
    {
        if let linkUrl = URL.init(string: urlString)
        {
            let safariViewController = SFSafariViewController(url: linkUrl)
            safariViewController.delegate = self
            self.present(safariViewController, animated: true, completion: nil)
        }
    }

    func safariViewControllerDidFinish(_ controller: SFSafariViewController)
    {
        controller.dismiss(animated: true, completion: nil)
    }

}

class ToplistCell: UITableViewCell {
    //Outlets
    @IBOutlet weak var orderNoLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var plogsLabel: UILabel!
}
