//
//  ChangePasswordViewController.swift
//  Plogge
//
//  Created by Mike Grankin on 5/30/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import UIKit
import Async

class ChangePasswordViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var oldPasswordTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var passwordConfirmTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        oldPasswordTextField.delegate = self
        passwordTextField.delegate = self
        passwordConfirmTextField.delegate = self
    }
    
    @IBAction func saveButtonPressed(_ sender: AnyObject) {
        view.endEditing(true)
        
        var alertText = ""
        
        if oldPasswordTextField.text!.isEmpty || passwordTextField.text!.isEmpty || passwordConfirmTextField.text!.isEmpty
        {
            alertText += "emptyPasswordMessage".localized()
        }
        else if passwordTextField.text != passwordConfirmTextField.text!
        {
            alertText += "differentPasswordMessage".localized()
        }
        
        if alertText == "" {
            Auth.instance.change(oldPassword: oldPasswordTextField.text!, toNewPassword: passwordTextField.text!){ (success, errorMessage) in
                if success
                {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    let alert = UIAlertController(title: "error".localized(), message: errorMessage, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        else
        {
            let alert = UIAlertController(title: "error".localized(), message: alertText, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        Async.main {
            if textField == self.oldPasswordTextField {
                self.passwordTextField.becomeFirstResponder()
            }
            else if textField == self.passwordTextField {
                self.passwordConfirmTextField.becomeFirstResponder()
            }
            else if textField == self.passwordConfirmTextField {
                //                self.registerButtonPressed(textField)
                self.dismissKeyboard()
            }
        }
        return true
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
}
