//
//  ScoreboardViewController.swift
//  Plogge
//
//  Created by Mike Grankin on 5/21/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import UIKit
import Async
import SafariServices

class ScoreboardViewController: UIViewController, SFSafariViewControllerDelegate {
    @IBOutlet weak var usersLabel: UILabel!
    @IBOutlet weak var ploggsLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        Scoreboard.instance.stats{ (success, errorMessage) in
            if success
            {
                Async.main{
                    self.usersLabel.text = String(Scoreboard.instance.totalUsers)
                    self.ploggsLabel.text = String(Scoreboard.instance.totalPlogs)
                    self.distanceLabel.text = Scoreboard.instance.totalDistance.kmFormatted
                }
            }
            else
            {
                let alert = UIAlertController(title: "error".localized(), message: errorMessage, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    @IBAction func didTapFriidrettButton(_ sender: Any) {
        self.handleURL("https://www.friidrett.no/")
    }
    
    @IBAction func didTapGjensidigeButton(_ sender: Any) {
        self.handleURL("https://www.gjensidige.no/")
    }
    
    
    func handleURL(_ urlString:String)
    {
        if let linkUrl = URL.init(string: urlString)
        {
            let safariViewController = SFSafariViewController(url: linkUrl)
            safariViewController.delegate = self
            self.present(safariViewController, animated: true, completion: nil)
        }
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController)
    {
        controller.dismiss(animated: true, completion: nil)
    }

}
