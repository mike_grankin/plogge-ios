//
//  ChangeUsernameViewController.swift
//  Plogge
//
//  Created by Mike Grankin on 5/30/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import UIKit
import Async

class ChangeUsernameViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var usernameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        usernameTextField.delegate = self
        usernameTextField.text = Auth.instance.profileUsername
    }
    
    @IBAction func saveButtonPressed(_ sender: AnyObject) {
        view.endEditing(true)
        
        var alertText = ""
        
        if usernameTextField.text!.isEmpty{
            alertText += "emptyUsernameMessage".localized()
        }

        if alertText == "" {
            Auth.instance.change(newUsername: usernameTextField.text!){ (success, errorMessage) in
                if success
                {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    let alert = UIAlertController(title: "error".localized(), message: errorMessage, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        else
        {
            let alert = UIAlertController(title: "error".localized(), message: alertText, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized(), style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        Async.main {
            if textField == self.usernameTextField {
                self.dismissKeyboard()
            }
        }
        return true
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
}
