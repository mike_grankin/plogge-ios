//
//  Constants.swift
//  Plogge
//
//  Created by Mike Grankin on 4/27/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

struct Constants {
//    static let GoogleAPIKey = "AIzaSyDR336BdsMDfy77z4tTF6ZinwHnQV2gYuA"
    static let GoogleClientID = "717102350626-8321m1eb229qkuv47ee3co3cfuj4huin.apps.googleusercontent.com"
//    static let OneSignalAppID = "fad18a78-6788-4f07-a66b-cd5d38b619e3"
    static let itunesLink = "https://itunes.apple.com/us/app/plogger/id1377114404?ls=1&mt=8"
    static let baseURLProd = "https://api.plogge.no/"
    static let baseURLTest = "https://plogge.ways.fi/"
    static let tokenHeaderPrefix = "Bearer "
//    static let deepLinksBaseURL = "https://woopnews.com/article/"
//    static let universalLinkArticleIndex = 0
//    static let universalLinkArticleIDIndex = 1
//    static let universalLinkArticle = "article"
    
}

