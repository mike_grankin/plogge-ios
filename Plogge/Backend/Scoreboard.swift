//
//  Scoreboard.swift
//  Plogge
//
//  Created by Mike Grankin on 5/24/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import Alamofire
import SwiftyJSON

class Scoreboard: NSObject {
    
    static var instance = Scoreboard()
    var totalUsers = 0
    var totalPlogs = 0
    var totalDistance = 0
    var toplistItems = Array<ToplistItem>()
    var toplistUserRanking = 0
    var toplistTotalUsers = 0
    var toplistUserCommune = ""
    var toplistCommunePlogs = 0
    
    override init(){
        super.init()
    }
    
    func stats(completion:@escaping (_ success: Bool, _ message: String?) -> Void) {
        let url = AppConfig.instance.baseURL + "v1/scoreboard/stats"
        
        Alamofire.request(url).responseJSON { response in
            
            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)
                
                guard json["error"].arrayValue.count == 0 else {
                    let message = json["error"][0]["error"].string
                    completion(false, message)
                    return
                }
                
                self.totalUsers = json["data"]["total_users"].intValue
                self.totalPlogs = json["data"]["total_plogs"].intValue
                self.totalDistance = json["data"]["total_distance"].intValue
                completion(true, nil)
            }
            else
            {
                let error = response.error
                completion(false, error?.localizedDescription)
            }
        }
    }
    
    func toplist(completion:@escaping (_ success: Bool, _ message: String?) -> Void) {
        let url = AppConfig.instance.baseURL + "v1/scoreboard/toplist"
        
        Alamofire.request(url).responseJSON { response in
            
            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)
                
                guard json["error"].arrayValue.count == 0 else {
                    let message = json["error"][0]["error"].string
                    completion(false, message)
                    return
                }
                
                var toplistItems = Array<ToplistItem>()
                if let toplistItemsJSON = json["data"]["scoreboard"].array
                {
                    for toplistItemJSON in toplistItemsJSON
                    {
                        if let toplistItem = ToplistItem(jsonRaw: toplistItemJSON)
                        {
                            toplistItems.append(toplistItem)
                        }
                    }
                    self.toplistItems = toplistItems.sorted{$0.plogs > $1.plogs}
                }
                
                self.toplistUserRanking = json["data"]["user_ranking"].intValue
                self.toplistTotalUsers = json["data"]["total_users"].intValue
                self.toplistUserCommune = json["data"]["user_commune"].stringValue
                self.toplistCommunePlogs = json["data"]["commune_plogs"].intValue

                completion(true, nil)
            }
            else
            {
                let error = response.error
                completion(false, error?.localizedDescription)
            }
        }
    }

}
