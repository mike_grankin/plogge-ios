//
//  Plog.swift
//  Plogge
//
//  Created by Mike Grankin on 5/15/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import Alamofire
import SwiftyJSON
import CoreLocation

class Plog: NSObject {
    static var instance = Plog()
    var currentTripID:Int?
    var funFacts = Array<FunFact>()
    var currentPlogs = Array<PloggItem>()
    var ploggHistoryItems = Array<PloggHistoryItem>()
    
    override init(){
        super.init()
    }
    
    func start(_ currentLocation:CLLocationCoordinate2D, completion:@escaping (_ success: Bool, _ message: String?) -> Void) {
        let url = AppConfig.instance.baseURL + "v1/trip/start"
        
        //Make parameter JSON
        let parameters: [String:AnyObject] = [
            "start_time":Date().iso8601 as AnyObject,
            "latitude":currentLocation.latitude as AnyObject,
            "longitude":currentLocation.longitude as AnyObject
        ]

        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)

                guard json["error"].arrayValue.count == 0 else {
                    let message = json["error"][0]["error"].string
                    completion(false, message)
                    return
                }
                
                self.currentTripID = json["data"]["trip_id"].int
                self.currentPlogs = Array<PloggItem>()
                
                var funFacts = Array<FunFact>()
                if let funFactsJSON = json["data"]["fun_facts"].array
                {
                    for funFactJSON in funFactsJSON
                    {
                        if let funFact = FunFact(jsonRaw: funFactJSON)
                        {
                            funFacts.append(funFact)
                        }
                    }
                    self.funFacts = funFacts.sorted{$0.afterPlog < $1.afterPlog}
                }

                completion(true, nil)
            }
            else
            {
                let error = response.error
                completion(false, error?.localizedDescription)
            }
        }
    }
    
    func stop(_ currentLocation:CLLocationCoordinate2D, _ distance: Int, completion:@escaping (_ success: Bool, _ message: String?) -> Void) {
        guard let tripID = self.currentTripID else { return }
        let url = AppConfig.instance.baseURL + "v1/trip/\(tripID)/end"
        
        //Make parameter JSON
        var ploggs = [AnyObject]()
        for plogg in currentPlogs
        {
            let ploggRAW: [String:AnyObject] = [
                "add_time":plogg.date.iso8601 as AnyObject,
                "latitude":plogg.coordinates.latitude as AnyObject,
                "longitude":plogg.coordinates.longitude as AnyObject,
                "quantity":plogg.count as AnyObject
            ]

            ploggs.append(ploggRAW as AnyObject)
        }

        
        let parameters: [String:AnyObject] = [
            "end_time":Date().iso8601 as AnyObject,
            "latitude":currentLocation.latitude as AnyObject,
            "longitude":currentLocation.longitude as AnyObject,
            "distance":distance as AnyObject,
            "plogs": ploggs as AnyObject
        ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)
                
                guard json["error"].arrayValue.count == 0 else {
                    let message = json["error"][0]["error"].string
                    completion(false, message)
                    return
                }
                
                self.currentTripID = 0
                self.currentPlogs = Array<PloggItem>()
                let message = json["data"]["fun_fact_message"].string
                completion(true, message)
            }
            else
            {
                let error = response.error
                completion(false, error?.localizedDescription)
            }
        }
    }
    
    func addPlogg(_ plogg: PloggItem)
    {
        self.currentPlogs.append(plogg)
    }
    
    func removeLastPlogg() -> Int?
    {
        if let lastItemPloggsCount = self.currentPlogs.last?.count
        {
            self.currentPlogs.removeLast()
            return lastItemPloggsCount
        }
        else
        {
            return nil
        }
    }

    
    func delete(_ ploggHistoryItem:PloggHistoryItem, completion:@escaping (_ success: Bool, _ message: String?) -> Void) {
        let url = AppConfig.instance.baseURL + "v1/trip/\(ploggHistoryItem.id)"
        
        Alamofire.request(url, method: .delete, parameters: nil, encoding: JSONEncoding.default).responseJSON { response in
            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)
                
                guard json["error"].arrayValue.count == 0 else {
                    let message = json["error"][0]["error"].string
                    completion(false, message)
                    return
                }
                
                self.ploggHistoryItems = self.ploggHistoryItems.filter({$0.id != ploggHistoryItem.id})
                completion(true, nil)
            }
            else
            {
                let error = response.error
                completion(false, error?.localizedDescription)
            }
        }
    }

    func history(completion:@escaping (_ success: Bool, _ message: String?) -> Void) {
        let url = AppConfig.instance.baseURL + "v1/trips/0"
        
        Alamofire.request(url).responseJSON { response in
            
            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)
                
                guard json["error"].arrayValue.count == 0 else {
                    let message = json["error"][0]["error"].string
                    completion(false, message)
                    return
                }
                
                var ploggHistoryItems = Array<PloggHistoryItem>()
                if let ploggHistoryItemsJSON = json["data"]["data"].array
                {
                    for ploggHistoryItemJSON in ploggHistoryItemsJSON
                    {
                        if let ploggHistoryItem = PloggHistoryItem(jsonRaw: ploggHistoryItemJSON)
                        {
                            ploggHistoryItems.append(ploggHistoryItem)
                        }
                    }
                    self.ploggHistoryItems = ploggHistoryItems.sorted{$0.startTime > $1.startTime}
                }


                completion(true, nil)
            }
            else
            {
                let error = response.error
                completion(false, error?.localizedDescription)
            }
        }
    }
    
    func getFunFact(afterPlog plogNumber:Int) -> String?
    {
        if let funFact = self.funFacts.filter({$0.afterPlog <= plogNumber}).last
        {
            self.funFacts = self.funFacts.filter({$0.afterPlog > plogNumber})
            return funFact.message
        }
        else
        {
            return nil
        }
    }

}
