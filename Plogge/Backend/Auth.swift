//
//  Auth.swift
//  Plogge
//
//  Created by Mike Grankin on 5/14/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import Alamofire
import KeychainSwift
import SwiftyJSON
import FacebookCore
import GoogleSignIn

enum LoginVerifyResult : Int {
    case noMemebrshipFound
    case tryLoginAgain
    case tokenIsNotEmtpy
    case needAccountConfirmation
    case error
}

class Auth: NSObject {
    static var instance = Auth()
    let keychain = KeychainSwift()
    var token: String?
    {
        didSet{
            if token != nil
            {
                keychain.set(token!, forKey: "token", withAccess: .accessibleAlways)
            }
            else
            {
                keychain.delete("token")
            }
        }
    }
    
    var tokenForHeader: String?
    {
        if let token = token
        {
            return Constants.tokenHeaderPrefix + token
        }
        return nil
    }

    var email: String?
    {
        didSet{
            if email != nil
            {
                keychain.set(email!, forKey: "email", withAccess: .accessibleAlways)
            }
            else
            {
                keychain.delete("email")
            }
        }
    }
    
    var profileId = 0
    var profileUsername = ""
    var profileEmail = ""
    var profileCreatedAt = Date()
    var profileTotalDistance = 0
    var profileTotalPlogs = 0
    
    override init(){
        super.init()
        token = keychain.get("token")
        email = keychain.get("email")
    }
    
    func isUserLoggedIn() -> Bool {
        let hasToken = self.token != nil
        if !hasToken
        {
            return false
        }
        return true
    }
    
    func register(username: String, email: String, password: String, completion:@escaping (_ success: Bool, _ showTermsScreen: Bool, _ message: String?) -> Void) {
        
        //Make parameter JSON
        let parameters: [String:AnyObject] = [
            "username":username as AnyObject,
            "email":email as AnyObject,
            "password":password as AnyObject
        ]
        
        Alamofire.request(AppConfig.instance.baseURL + "v1/registration", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            
            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)
                
                guard json["error"].arrayValue.count == 0 else {
                    let message = json["error"][0]["error"].string
                    completion(false, false, message)
                    return
                }
                
                if let token = json["data"]["token"].string
                {
                    let showTermsScreen = json["data"]["showTermsScreen"].boolValue
                    self.token = token
                    self.email = email
                    completion(true, showTermsScreen, nil)
                }
                else
                {
                    self.token = nil
                    self.email = nil
                    completion(false, false, "defaultErrorMessage".localized())
                }
            }
            else
            {
                self.token = nil
                self.email = nil
                let error = response.error
                completion(false, false, error?.localizedDescription)
            }
        }
    }
    
    func login(_ email: String, _ password: String, completion:@escaping (_ success: Bool, _ showTermsScreen: Bool, _ message: String?) -> Void) {
        
        //Make parameter JSON
        let parameters: [String:AnyObject] = [
            "email":email as AnyObject,
            "password":password as AnyObject
        ]
        
        Alamofire.request(AppConfig.instance.baseURL + "v1/login", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)
                
                guard json["error"].arrayValue.count == 0 else {
                    self.token = nil
                    self.email = nil
                    let message = json["error"][0]["error"].string
                    completion(false, false, message)
                    return
                }
                
                if let token = json["data"]["token"].string
                {
                    let showTermsScreen = json["data"]["showTermsScreen"].boolValue
                    self.token = token
                    self.email = email
                    completion(true, showTermsScreen, nil)
                }
                else
                {
                    self.token = nil
                    self.email = nil
                    completion(false, false, "defaultErrorMessage".localized())
                }
            }
            else
            {
                //                self.currentUser = nil
                let error = response.error
                completion(false, false, error?.localizedDescription)
            }
        }
    }
    
    func loginWithFB(_ accessToken:AccessToken, _ userDict: Dictionary<String, Any>, completion:@escaping (_ success: Bool, _ showTermsScreen: Bool, _ message: String?) -> Void) {
        let email = userDict["email"] as! String
        //Make parameter JSON
        var parameters: [String:AnyObject] = [
            "token":accessToken.authenticationToken as AnyObject,
            "type":"facebook" as AnyObject,
            "username":userDict["name"] as AnyObject,
            "email":email as AnyObject
        ]
        
        if let picRaw = userDict["picture"] as? Dictionary<String, Any>,
            let picData = picRaw["data"] as? Dictionary<String, Any>,
            let picURL = picData["url"]
        {
            parameters["profile_image"] = picURL as AnyObject
        }
        
        Alamofire.request(AppConfig.instance.baseURL + "v1/login/social", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)
                
                guard json["error"].arrayValue.count == 0 else {
                    self.token = nil
                    self.email = nil
                    let message = json["error"][0]["error"].string
                    completion(false, false, message)
                    return
                }
                
                if let token = json["data"]["token"].string
                {
                    let showTermsScreen = json["data"]["showTermsScreen"].boolValue
                    self.token = token
                    self.email = email
                    completion(true, showTermsScreen, nil)
                }
                else
                {
                    self.token = nil
                    self.email = nil
                    completion(false, false, "defaultErrorMessage".localized())
                }
            }
            else
            {
                //                self.currentUser = nil
                let error = response.error
                completion(false, false, error?.localizedDescription)
            }
        }
    }
    
    func loginWithGoogle(_ googleUser:GIDGoogleUser, completion:@escaping (_ success: Bool, _ showTermsScreen: Bool, _ message: String?) -> Void) {
        //Make parameter JSON
        var parameters: [String:AnyObject] = [
            "token":googleUser.authentication.idToken as AnyObject,
            "type":"google" as AnyObject,
            "username":googleUser.profile.name as AnyObject,
            "email":googleUser.profile.email as AnyObject
        ]
        
        if googleUser.profile.hasImage,
           let profileImageURL = googleUser.profile.imageURL(withDimension: 200)
        {
            parameters["profile_image"] = profileImageURL.absoluteString as AnyObject
        }
        
        Alamofire.request(AppConfig.instance.baseURL + "v1/login/social", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)
                
                guard json["error"].arrayValue.count == 0 else {
                    self.token = nil
                    self.email = nil
                    let message = json["error"][0]["error"].string
                    GIDSignIn.sharedInstance().signOut()
                    completion(false, false, message)
                    return
                }
                
                if let token = json["data"]["token"].string
                {
                    let showTermsScreen = json["data"]["showTermsScreen"].boolValue
                    self.token = token
                    self.email = googleUser.profile.email
                    completion(true, showTermsScreen, nil)
                }
                else
                {
                    GIDSignIn.sharedInstance().signOut()
                    self.token = nil
                    self.email = nil
                    completion(false, false, "defaultErrorMessage".localized())
                }
            }
            else
            {
                //                self.currentUser = nil
                GIDSignIn.sharedInstance().signOut()
                let error = response.error
                completion(false, false, error?.localizedDescription)
            }
        }
    }
    
    func forgotPassword(email: String, completion:@escaping (_ success: Bool, _ message: String?) -> Void) {
        let url = AppConfig.instance.baseURL + "v1/login/\(email)/reset"

        Alamofire.request(url).responseJSON { response in

            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)

                guard json["error"].arrayValue.count == 0 else {
                    let message = json["error"][0]["error"].string
                    completion(false, message)
                    return
                }

                completion(true, nil)
            }
            else
            {
                let error = response.error
                completion(false, error?.localizedDescription)
            }
        }
    }

    func change(oldPassword: String, toNewPassword newPassword: String, completion:@escaping (_ success: Bool, _ message: String?) -> Void) {
        guard let token = Auth.instance.tokenForHeader else { return }
        let url = AppConfig.instance.baseURL + "v1/profile/changePassword"

        //Make parameter JSON
        let parameters: [String:AnyObject] = [
            "old_password":oldPassword as AnyObject,
            "new_password":newPassword as AnyObject
        ]

        //Make headers
        let headers: HTTPHeaders = ["Authorization" : token,
                                    "Content-Type" : "application/json"]

        Alamofire.request(url, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)

                guard json["error"].arrayValue.count == 0 else {
                    let message = json["error"][0]["error"].string
                    completion(false, message)
                    return
                }

                completion(true, nil)
            }
            else
            {
                //                self.currentUser = nil
                let error = response.error
                completion(false, error?.localizedDescription)
            }
        }
    }

    func change(newUsername: String, completion:@escaping (_ success: Bool, _ message: String?) -> Void) {
        guard let token = Auth.instance.tokenForHeader else { return }
        let url = AppConfig.instance.baseURL + "v1/profile"

        //Make parameter JSON
        let parameters: [String:AnyObject] = [
            "username":newUsername as AnyObject
        ]

        //Make headers
        let headers: HTTPHeaders = ["Authorization" : token,
                                    "Content-Type" : "application/json"]

        Alamofire.request(url, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)

                guard json["error"].arrayValue.count == 0 else {
                    let message = json["error"][0]["error"].string
                    completion(false, message)
                    return
                }

                self.profileUsername = newUsername
                completion(true, nil)
            }
            else
            {
                //                self.currentUser = nil
                let error = response.error
                completion(false, error?.localizedDescription)
            }
        }
    }

//    func change(newProfileImge: UIImage, completion:@escaping (_ success: Bool, _ message: String?) -> Void) {
//        guard let token = Auth.instance.tokenForHeader else { return }
//        let url = AppConfig.instance.baseURL + "v1/profile/return"
//
//        //Make parameter JSON
//
//        let imageData = UIImagePNGRepresentation(newProfileImge)!
//        let strBase64 = imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
//
//        let parameters: [String:AnyObject] = [
//            "profile_image":strBase64 as AnyObject
//        ]
//
//        //Make headers
//        let headers: HTTPHeaders = ["Authorization" : token,
//                                    "Content-Type" : "application/json"]
//
//        Alamofire.request(url, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
//            if response.result.isSuccess,
//                let value = response.result.value
//            {
//                let json = JSON(value)
//
//                guard json["error"].arrayValue.count == 0 else {
//                    let message = json["error"][0]["error"].string
//                    completion(false, message)
//                    return
//                }
//
//
//                if let imageProfile = json["data"]["profile_image"].string,
//                    let imageProfileUTF8 = imageProfile.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
//                    let imageProfileUrl = URL.init(string: imageProfileUTF8)
//                {
//                    Profile.instance.currentUser?.imageProfile = imageProfileUrl
//                }
//
//
//                completion(true, nil)
//            }
//            else
//            {
//                //                self.currentUser = nil
//                let error = response.error
//                completion(false, error?.localizedDescription)
//            }
//        }
//    }
//
//
    func acceptTerms(_ accept:Bool, completion:@escaping (_ success: Bool, _ message: String?) -> Void) {
        let url = AppConfig.instance.baseURL + "v1/acceptTerms/\(accept ? 1 : 0))"

        Alamofire.request(url).responseJSON { response in

            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)

                guard json["error"].arrayValue.count == 0 else {
                    let message = json["error"][0]["error"].string
                    completion(false, message)
                    return
                }

                if !accept
                {
                    self.logOut(){ (success, errorMessage) in
                        if success
                        {
                            completion(true, nil)
                        }
                        else
                        {
                            completion(false, errorMessage)
                        }
                    }
                }
                else
                {
                    completion(true, nil)
                }
            }
            else
            {
                let error = response.error
                completion(false, error?.localizedDescription)
            }
        }
    }

    func logOut(completion:@escaping (_ success: Bool, _ message: String?) -> Void) {
        guard let _ = Auth.instance.token else { return }
        
        let url = AppConfig.instance.baseURL + "v1/logout"
        
        Alamofire.request(url).responseJSON { response in
            
            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)
                
                guard json["error"].arrayValue.count == 0 else {
                    let message = json["error"][0]["error"].string
                    completion(false, message)
                    return
                }
                
                GIDSignIn.sharedInstance().signOut()
                self.token = nil
                completion(true, nil)
            }
            else
            {
                let error = response.error
                completion(false, error?.localizedDescription)
            }
        }
    }
    
    func downloadProfile(completion:@escaping (_ success: Bool, _ message: String?) -> Void) {
        guard let _ = Auth.instance.token else { return }
        
        let url = AppConfig.instance.baseURL + "v1/profile"
        
        Alamofire.request(url).responseJSON { response in
            
            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)
                
                guard json["error"].arrayValue.count == 0 else {
                    let message = json["error"][0]["error"].string
                    completion(false, message)
                    return
                }

                self.profileId = json["data"]["id"].intValue
                self.profileUsername = json["data"]["username"].stringValue
                self.profileEmail = json["data"]["email"].stringValue
                let createdAtISO = json["data"]["created_at"].stringValue
                self.profileCreatedAt = createdAtISO.dateFromISO8601!
                self.profileTotalDistance = json["data"]["total_distance"].intValue
                self.profileTotalPlogs = json["data"]["total_plogs"].intValue
                
                completion(true, nil)
            }
            else
            {
                let error = response.error
                completion(false, error?.localizedDescription)
            }
        }
    }
    
    func deleteAccount(completion:@escaping (_ success: Bool, _ message: String?) -> Void) {
        guard let _ = Auth.instance.token else { return }
        
        let url = AppConfig.instance.baseURL + "v1/profile/deleteRequest"
        
        Alamofire.request(url).responseJSON { response in
            
            if response.result.isSuccess,
                let value = response.result.value
            {
                let json = JSON(value)
                
                guard json["error"].arrayValue.count == 0 else {
                    let message = json["error"][0]["error"].string
                    completion(false, message)
                    return
                }
                
                GIDSignIn.sharedInstance().signOut()
                self.token = nil
                completion(true, nil)
            }
            else
            {
                let error = response.error
                completion(false, error?.localizedDescription)
            }
        }
    }
    
}
