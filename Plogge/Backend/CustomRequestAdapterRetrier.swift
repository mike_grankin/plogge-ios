//
//  CustomRequestAdapterRetrier.swift
//  Plogge
//
//  Created by Mike Grankin on 5/14/18.
//  Copyright © 2018 Ways AS. All rights reserved.
//

import Alamofire

class HeadersRequestAdapter: RequestAdapter {
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        
        if let accessToken = Auth.instance.tokenForHeader
        {
            urlRequest.setValue(accessToken, forHTTPHeaderField: "Authorization")
        }
        
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        return urlRequest
    }
}


//class CustomRequestRetrier: RequestRetrier {
//    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
//        if let response = request.task?.response as? HTTPURLResponse,
//            response.statusCode == 498
//        {
//            NotificationCenter.default.post(name: Notification.Name(rawValue: "logOutBy498"), object: nil, userInfo: nil)
//        }
//        completion(false, 0.0)
//    }
//}
